import { Cards } from "./cards.js";
import { State } from "./game.state.js";

export class Game {
  constructor() {
    //Créer la partie ou la charger

    $(document).on("click", ".btn", event => {
      this.StartGame(event);
    });
    $("body").on("click", ".image", e => this.checkImage(e));
  }

  refreshView() {
    Cards.update(this.json);
    State.update(this.json);
  }

  //Lancer le jeu
  StartGame() {
    this.api = "https://pierrefroumenty.com/dobble";
    this.pseudo = $("#name").val();
    this.room = $("#room_name").val();
    fetch(`${this.api}/join/${this.room}?pseudo=${this.pseudo}`)
      .then(response => {
        return response.json();
      })
      .then(json => {
        if (json.success) {
          console.log(json);
          toastr.success(`Bienvenue dans ${json.room}, le jeux commence`);
          this.json = json;
          this.refreshView();
        } else {
          toastr.error("La salle que vous essayez de rejoindre est pleine");
        }
      });
  }

  //Selection des cartes
  checkImage(e) {

    var id_img = e.currentTarget.getAttribute("data-id");
    const url = `https://pierrefroumenty.com/dobble/room/${
      this.json.room_id
    }/play/${id_img}?player_id=${this.json.player_id}`;

    fetch(url)
      .then(response => {
        return response.json();
      })
      .then(json => {
        if (json.success) {
          this.json = json;
          this.refreshView();
        } else {
          toastr.error(`Mauvaise Carte. Une carte est ajoutée à ton deck`);
        }
      });
  }
}
