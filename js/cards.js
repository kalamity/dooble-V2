//Class Carte qui regroupe toutes les actions effectuées sur les cartes
export class Cards {
  //Exporter vers Game
  static update (json) {
    Cards.getCardServer(json);
    Cards.getCardPlayer(json);
  }
  //Fonction qui récupére les cartes du serveur
  static getCardServer(json) {
    //Les cartes serveurs sont affichées dans la div #server
    $("#server").html(`<div class="colm"><div class="image">
    <img src="./assets/png/${
      json.stack[0].image
    }.png" style="transform:rotate(${json.stack[0].rotate}deg) scale(${
      json.stack[0].scale
    });">
  </div>
  </div>
  <div class="colm">
  <div class="image">
    <img src="./assets/png/${
      json.stack[1].image
    }.png" style="transform:rotate(${json.stack[1].rotate}deg) scale(${
      json.stack[1].scale
    });">
  </div>
  <div class="image">
   <img src="./assets/png/${json.stack[2].image}.png" style="transform:rotate(${
      json.stack[2].rotate
    }deg) scale(${json.stack[2].scale});">
  </div>
  <div class="image">
    <img src="./assets/png/${
      json.stack[3].image
    }.png" style="transform:rotate(${json.stack[3].rotate}deg) scale(${
      json.stack[3].scale
    });">
  </div>
  </div>
  <div class="colm">
  <div class="image">
    <img src="./assets/png/${
      json.stack[4].image
    }.png" style="transform:rotate(${json.stack[4].rotate}deg) scale(${
      json.stack[4].scale
    });">
  </div>
  </div>`);
  }
  //Fonction qui récupére les cartes de jeux du joueur).
  static getCardPlayer(json) {
    var player_id = json.player_id;
    var player = json.players.find(player => {
      return String(player.id) == player_id;
    });
  $("#player").html(`<div class="colm">
  <div class="image" data-id="${player.hand[0].image}">
  <img src="./assets/png/${
    player.hand[0].image
  }.png" style="transform:rotate(${player.hand[0].rotate}deg) scale(${player.hand[0].scale});">
  </div>
  </div>
  <div class="colm">
  <div class="image" data-id="${player.hand[1].image}">
  <img src="./assets/png/${
    player.hand[1].image
  }.png" style="transform:rotate(${player.hand[1].rotate}deg) scale(${player.hand[1].scale});">
  </div>
  <div class="image" data-id="${player.hand[2].image}">
  <img src="./assets/png/${
    player.hand[2].image
  }.png" style="transform:rotate(${player.hand[2].rotate}deg) scale(${player.hand[2].scale});">
  </div>
  <div class="image"data-id="${player.hand[3].image}">
  <img src="./assets/png/${
    player.hand[3].image
  }.png" style="transform:rotate(${player.hand[3].rotate}deg) scale(${player.hand[3].scale});">
  </div>
  </div>
  <div class="colm">
  <div class="image" data-id="${player.hand[4].image}">
  <img src="./assets/png/${
    player.hand[4].image
  }.png" style="transform:rotate(${player.hand[4].rotate}deg) scale(${player.hand[4].scale});">
  </div>
  </div>`);
  }


}


