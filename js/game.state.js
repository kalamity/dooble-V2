//Class Sate qui regroupes les mise à jours du jeux  : score, noms joueurs, nom de la salle ect...
export class State {
  static update(json) {
    State.getRoom(json);
    State.getInfo(json);
  }
  static getRoom(json) {
    console.log(json.room);
    $("#gameroom").html(`<h3 class="title">${json.room}</h3>`);
  }
  static getInfo(json) {
    console.log(json.players);
    for (var i = 0; i < json.players.length; i++) {
      $("#score").html(`<div class="player-other">
            <span>${json.players[i].pseudo}</span>
            <span class="score">${json.players[i].nb_cards}/12</span>
        </div
    </div>`);
    }
  }
}
